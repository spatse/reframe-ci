# ReFrame test pipeline for the
# Advanced Tri-Lab Software Environment (ATSE)
# Written and maintained by Carson Woods

import reframe as rfm
import reframe.utility.sanity as sn
from reframe.core.launchers import LauncherWrapper

import os

@rfm.simple_test
class Hello_World_C(rfm.RegressionTest):
    """
    Compile a simple c program
    and test execution output
    """
    valid_systems = ['*:local-build', '*:slurm-build']
    valid_prog_environs = ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
    sourcepath = 'hello.c'

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found(r'Hello, World\!', self.stdout)


@rfm.simple_test
class Hello_World_CPP(rfm.RegressionTest):
    """
    Compile a simple cpp program
    and test execution output
    """
    valid_systems = ['*:local-build', '*:slurm-build']
    valid_prog_environs = ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
    sourcepath = 'hello.cpp'

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found(r'Hello, World\!', self.stdout)


@rfm.simple_test
class Hello_Threaded_CPP(rfm.RegressionTest):
    """
    Compile a multithreaded cpp program
    using pthread and test execution output
    """
    valid_systems = ['*:local-build', '*:slurm-build']
    valid_prog_environs = ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
    sourcepath = 'hello_threads.cpp'
    build_system = 'SingleSource'
    executable_opts = ['16']

    @run_before('compile')
    def set_compilation_flags(self):
        self.build_system.cppflags = ['-DSYNC_MESSAGES']
        self.build_system.cxxflags = ['-std=c++11', '-Wall']
        environ = self.current_environ.name
        if environ in {'clang', 'gnu7', 'gnu10', 'arm20.3', '21.1'}:
            self.build_system.cxxflags += ['-pthread']

    @run_before('sanity')
    def set_sanity_patterns(self):
        num_messages = sn.len(sn.findall(r'\[\s?\d+\] Hello, World\!',
                                         self.stdout))
        self.sanity_patterns = sn.assert_eq(num_messages, 16)


@rfm.simple_test
class Hello_World_CUDA(rfm.RunOnlyRegressionTest):
    """
    Compile a simple cuda program
    and test execution output
    """
    valid_systems = ['*:cuda']
    valid_prog_environs = ['*']
    executable = 'nvcc'
    executable_opts = [
        'hello.cu',  '-o ./Hello_World_CUDA'
    ]
    prerun_cmds = [
        'export PATH=$PATH:/usr/local/cuda-10.2/bin/'
    ]
    postrun_cmds = [
        './Hello_World_CUDA'
    ]

    extra_resources = {'partition': {'partition': 'GPU'},
                       'time': {'time': '0-04:00:00'}}

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found(r'final result: 1.000000', self.stdout)


@rfm.simple_test
class Init_MPI(rfm.RegressionTest):
    """
    Test if MPI can be initialized by running
    a hello world program on openmpi3 and openmpi4
    """
    valid_systems = ['*:mpi']
    valid_prog_environs = ['openmpi3', 'openmpi4']
    sourcepath = 'mpi_hello_world.c'

    @run_before('run')
    def set_core_opts(self):
        self.job.launcher.options = ['-n', '4']

    @run_before('sanity')
    def set_sanity_patterns(self):
        num_messages = sn.len(sn.findall(r'Hello from processor \d+ of \d+', self.stdout))
        self.sanity_patterns = sn.assert_eq(num_messages, 4)


@rfm.simple_test
class MPI_Ring(rfm.RegressionTest):
    """
    Compile and execute an MPI ring test and
    validate that output is as expected
    """
    valid_systems = ['*:mpi']
    valid_prog_environs = ['openmpi3', 'openmpi4']
    sourcepath = 'mpi_ring.c'

    @run_before('run')
    def set_core_opts(self):
        self.job.launcher.options = ['-n', '4']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found(r'Ring Complete', self.stdout)


@rfm.simple_test
class MPI_HCOLL(rfm.RunOnlyRegressionTest):
    """
    Ensures that HCOLL is enabled for collective operations
    Currently, a test is run just to ensure that IMB runs
    TODO: Add performance metrics in future iteration of this test
    NOTE: performance metrics are important as HCOLL often fails silently
          and performance is the primary metric for determining when failures
          occur
    """

    # needs slurm/launcher for collective operation support
    valid_systems = ['*:slurm-mpi']
    valid_prog_environs = ['*']

    # Request 50 nodes to ensure collective operations
    # have room to show off performance
    extra_resources = {'nodes': {'num_nodes': '8'},
                       'time': {'time': '0-04:00:00'}}

    num_tasks = 128
    executable = rfm.utility.osext.run_command('spack location -i /a3lrac4').stdout.rstrip()+'/bin/IMB-MPI1'

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_not_found('error', self.stderr)


@rfm.simple_test
class osu_hello(rfm.RunOnlyRegressionTest):
    """
    Runs osu_hello test and validates output
    """

    # needs slurm/launcher for collective operation support
    valid_systems = ['*:slurm-mpi']
    valid_prog_environs = ['openmpi3', 'openmpi4']

    # Request 50 nodes to ensure collective operations
    # have room to show off performance
    extra_resources = {'nodes': {'num_nodes': '8'},
                       'time': {'time': '0-04:00:00'}}
    num_tasks = 128

    modules = ['osu-micro-benchmarks']
    executable = 'osu_hello'

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.all([
            sn.assert_found(r'# OSU MPI Hello World Test', self.stdout),
            sn.assert_found(r'This is a test with 128 processes', self.stdout),
            sn.assert_not_found('error', self.stderr)
        ])


@rfm.simple_test
class osu_init(rfm.RunOnlyRegressionTest):
    """
    Runs osu_init test and validates output
    """

    # needs slurm/launcher for collective operation support
    valid_systems = ['*:slurm-mpi']
    valid_prog_environs = ['openmpi3', 'openmpi4']

    # Request 50 nodes to ensure collective operations
    # have room to show off performance
    extra_resources = {'nodes': {'num_nodes': '8'},
                       'time': {'time': '0-04:00:00'}}
    num_tasks = 128

    modules = ['osu-micro-benchmarks']
    executable = 'osu_init'

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.all([
            sn.assert_found(r'# OSU MPI Init Test', self.stdout),
            sn.assert_found(r'nprocs: 128', self.stdout),
            sn.assert_not_found('error', self.stderr)
        ])

    @run_before('performance')
    def set_perf_patterns(self):
        self.perf_patterns = {
            'Init Time (ms)': sn.extractsingle(r'avg:\s+(\S+)\s+.*',
                                     self.stdout, 1, float)
        }


@rfm.simple_test
class osu_scatter(rfm.RunOnlyRegressionTest):
    """
    Runs osu_scatter test and validates output
    """

    # needs slurm/launcher for collective operation support
    valid_systems = ['*:slurm-mpi']
    valid_prog_environs = ['openmpi3', 'openmpi4']

    # Request 50 nodes to ensure collective operations
    # have room to show off performance
    extra_resources = {'nodes': {'num_nodes': '8'},
                       'time': {'time': '0-04:00:00'}}
    num_tasks = 128

    modules = ['osu-micro-benchmarks']
    executable = 'osu_scatter'

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.all([
            sn.assert_found(r'# OSU MPI Scatter Latency Test', self.stdout),
            sn.assert_not_found('error', self.stderr)
        ])

    @performance_function('Bytes', perf_key='65536')
    def extract_small_size_perf(self):
        return sn.extractsingle(r'65536\s+(\S+)', self.stdout, 1, float)

    @performance_function('Bytes', perf_key='262144')
    def extract_med_size_perf(self):
        return sn.extractsingle(r'262144\s+(\S+)', self.stdout, 1, float)

    @performance_function('Bytes', perf_key='1048576')
    def extract_large_size_perf(self):
        return sn.extractsingle(r'1048576\s+(\S+)', self.stdout, 1, float)


@rfm.simple_test
class osu_allgather(rfm.RunOnlyRegressionTest):
    """
    Runs osu_allgather test and validates output
    """

    # needs slurm/launcher for collective operation support
    valid_systems = ['*:slurm-mpi']
    valid_prog_environs = ['openmpi3', 'openmpi4']

    # Request 50 nodes to ensure collective operations
    # have room to show off performance
    extra_resources = {'nodes': {'num_nodes': '8'},
                       'time': {'time': '0-04:00:00'}}
    num_tasks = 128

    modules = ['osu-micro-benchmarks']
    executable = 'osu_allgather'

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.all([
            sn.assert_found(r'# OSU MPI Allgather Latency Test', self.stdout),
            sn.assert_not_found('exited with non-zero status', self.stderr)
        ])

    @performance_function('Bytes', perf_key='65536')
    def extract_small_size_perf(self):
        return sn.extractsingle(r'65536\s+(\S+)', self.stdout, 1, float)

    @performance_function('Bytes', perf_key='262144')
    def extract_med_size_perf(self):
        return sn.extractsingle(r'262144\s+(\S+)', self.stdout, 1, float)

    @performance_function('Bytes', perf_key='1048576')
    def extract_large_size_perf(self):
        return sn.extractsingle(r'1048576\s+(\S+)', self.stdout, 1, float)


@rfm.simple_test
class osu_allreduce(rfm.RunOnlyRegressionTest):
    """
    Runs osu_allreduce test and validates output
    """

    # needs slurm/launcher for collective operation support
    valid_systems = ['*:slurm-mpi']
    valid_prog_environs = ['openmpi3', 'openmpi4']

    # Request 50 nodes to ensure collective operations
    # have room to show off performance
    extra_resources = {'nodes': {'num_nodes': '8'},
                       'time': {'time': '0-04:00:00'}}
    num_tasks = 128

    modules = ['osu-micro-benchmarks']
    executable = 'osu_allreduce'

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.all([
            sn.assert_found(r'# OSU MPI Allreduce Latency Test', self.stdout),
            sn.assert_not_found('exited with non-zero status', self.stderr)
        ])

    @performance_function('Bytes', perf_key='65536')
    def extract_small_size_perf(self):
        return sn.extractsingle(r'65536\s+(\S+)', self.stdout, 1, float)

    @performance_function('Bytes', perf_key='262144')
    def extract_med_size_perf(self):
        return sn.extractsingle(r'262144\s+(\S+)', self.stdout, 1, float)

    @performance_function('Bytes', perf_key='1048576')
    def extract_large_size_perf(self):
        return sn.extractsingle(r'1048576\s+(\S+)', self.stdout, 1, float)


@rfm.simple_test
class osu_put_latency(rfm.RunOnlyRegressionTest):
    """
    Runs osu_put_latency test and validates output
    """

    # needs slurm/launcher for collective operation support
    valid_systems = ['*:slurm-mpi']
    valid_prog_environs = ['openmpi3', 'openmpi4']

    num_tasks = 2

    modules = ['osu-micro-benchmarks']
    executable = 'osu_put_latency'

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.all([
            sn.assert_found(r'# OSU MPI_Put Latency Test', self.stdout),
            sn.assert_not_found('non-zero exit code', self.stderr)
        ])

    @performance_function('Bytes', perf_key='65536')
    def extract_small_size_perf(self):
        return sn.extractsingle(r'65536\s+(\S+)', self.stdout, 1, float)

    @performance_function('Bytes', perf_key='262144')
    def extract_med_size_perf(self):
        return sn.extractsingle(r'262144\s+(\S+)', self.stdout, 1, float)

    @performance_function('Bytes', perf_key='1048576')
    def extract_large_size_perf(self):
        return sn.extractsingle(r'1048576\s+(\S+)', self.stdout, 1, float)

    @performance_function('Bytes', perf_key='4194304')
    def extract_extra_large_size_perf(self):
        return sn.extractsingle(r'4194304\s+(\S+)', self.stdout, 1, float)


@rfm.simple_test
class osu_get_latency(rfm.RunOnlyRegressionTest):
    """
    Runs osu_get_latency test and validates output
    """

    # needs slurm/launcher for collective operation support
    valid_systems = ['*:slurm-mpi']
    valid_prog_environs = ['openmpi3', 'openmpi4']

    num_tasks = 2

    modules = ['osu-micro-benchmarks']
    executable = 'osu_get_latency'

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.all([
            sn.assert_found(r'# OSU MPI_Get latency Test', self.stdout),
            sn.assert_not_found('non-zero exit code', self.stderr)
        ])

    @performance_function('Bytes', perf_key='65536')
    def extract_small_size_perf(self):
        return sn.extractsingle(r'65536\s+(\S+)', self.stdout, 1, float)

    @performance_function('Bytes', perf_key='262144')
    def extract_med_size_perf(self):
        return sn.extractsingle(r'262144\s+(\S+)', self.stdout, 1, float)

    @performance_function('Bytes', perf_key='1048576')
    def extract_large_size_perf(self):
        return sn.extractsingle(r'1048576\s+(\S+)', self.stdout, 1, float)

    @performance_function('Bytes', perf_key='4194304')
    def extract_extra_large_size_perf(self):
        return sn.extractsingle(r'4194304\s+(\S+)', self.stdout, 1, float)


@rfm.simple_test
class osu_latency(rfm.RunOnlyRegressionTest):
    """
    Runs osu_latency test and validates output
    """

    # needs slurm/launcher for collective operation support
    valid_systems = ['*:slurm-mpi']
    valid_prog_environs = ['openmpi3', 'openmpi4']

    # Request 50 nodes to ensure collective operations
    # have room to show off performance
    extra_resources = {'nodes': {'num_nodes': '8'},
                       'time': {'time': '0-04:00:00'}}
    num_tasks = 2

    modules = ['osu-micro-benchmarks']
    executable = 'osu_latency'

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.all([
            sn.assert_found(r'# OSU MPI Latency Test', self.stdout),
            sn.assert_not_found('non-zero exit code', self.stderr)
        ])

    @performance_function('Bytes', perf_key='65536')
    def extract_small_size_perf(self):
        return sn.extractsingle(r'65536\s+(\S+)', self.stdout, 1, float)

    @performance_function('Bytes', perf_key='262144')
    def extract_med_size_perf(self):
        return sn.extractsingle(r'262144\s+(\S+)', self.stdout, 1, float)

    @performance_function('Bytes', perf_key='1048576')
    def extract_large_size_perf(self):
        return sn.extractsingle(r'1048576\s+(\S+)', self.stdout, 1, float)

    @performance_function('Bytes', perf_key='4194304')
    def extract_extra_large_size_perf(self):
        return sn.extractsingle(r'4194304\s+(\S+)', self.stdout, 1, float)

@rfm.simple_test
class osu_bibw(rfm.RunOnlyRegressionTest):
    """
    Runs osu_bibw test and validates output
    """

    # needs slurm/launcher for collective operation support
    valid_systems = ['*:slurm-mpi']
    valid_prog_environs = ['openmpi3', 'openmpi4']

    # Request 50 nodes to ensure collective operations
    # have room to show off performance
    extra_resources = {'nodes': {'num_nodes': '8'},
                       'time': {'time': '0-04:00:00'}}
    num_tasks = 2

    modules = ['osu-micro-benchmarks']
    executable = 'osu_bibw'

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.all([
            sn.assert_found(r'# OSU MPI Bi-Directional Bandwidth Test', self.stdout),
            sn.assert_not_found('non-zero exit code', self.stderr)
        ])

    @performance_function('MB/s', perf_key='65536')
    def extract_small_size_perf(self):
        return sn.extractsingle(r'65536\s+(\S+)', self.stdout, 1, float)

    @performance_function('MB/s', perf_key='262144')
    def extract_med_size_perf(self):
        return sn.extractsingle(r'262144\s+(\S+)', self.stdout, 1, float)

    @performance_function('MB/s', perf_key='1048576')
    def extract_large_size_perf(self):
        return sn.extractsingle(r'1048576\s+(\S+)', self.stdout, 1, float)

    @performance_function('MB/s', perf_key='4194304')
    def extract_extra_large_size_perf(self):
        return sn.extractsingle(r'4194304\s+(\S+)', self.stdout, 1, float)


@rfm.simple_test
class OpenBLAS_SGEMM(rfm.RegressionTest):
    """
    Compile an SGEMM test test
    using OpenBLAS and validate output
    """
    valid_systems = ['*:local-build', '*:slurm-build']
    valid_prog_environs = ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
    modules = ['openblas/0.3.4']
    sourcepath = 'blas_test.c'
    build_system = 'SingleSource'

    @run_before('compile')
    def set_compilation_flags(self):
        environ = self.current_environ.name
        if environ == 'gnu7':
            comp = 'gcc@7.2.0'
        elif environ == 'gnu10':
            comp = 'gcc@10.3.0'
        elif environ == 'arm20.3':
            comp = '%arm'
        elif environ == 'arm21.1':
            comp = '%arm'


        blas_path = rfm.utility.osext.run_command('spack location -i openblas@0.3.4 %'+comp).stdout.rstrip()

        self.build_system.ldflags = ['-I'+blas_path+'/include/', '-L'+blas_path+'/lib/',
                                     '-Wl,-rpath,'+blas_path+'lib',
                                     '-lopenblas', '-lgfortran']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found(r'[ 488.32, 477.38, 529.03\n 489.83, 480.8, 530.59\n 490.52, 480.73, 531.53 ]',
                                               self.stdout)


@rfm.simple_test
class superlu_download(rfm.RunOnlyRegressionTest):
    """
    Download a copy of SuperLU for a future test
    """
    descr = 'Download SuperLU for testing CMake building package'
    valid_systems = ['*:local']
    valid_prog_environs = ['gnu7', 'gnu10']
    executable = 'wget'
    executable_opts = [
        'https://github.com/xiaoyeli/superlu/archive/refs/tags/v5.2.2.tar.gz'
    ]
    postrun_cmds = [
        'tar xzf v5.2.2.tar.gz'
    ]

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_not_found('error', self.stderr)


@rfm.simple_test
class build_cmake_package(rfm.CompileOnlyRegressionTest):
    """
    Tests CMake functionality by building
    the SuperLU tar archive downloaded previously
    """
    descr = 'Test that a package can be built with CMake'
    valid_systems = ['*:local']
    valid_prog_environs = ['gnu7', 'gnu10']
    modules = ['cmake/3.20.5']
    build_system = 'CMake'

    @run_after('init')
    def inject_dependencies(self):
        self.depends_on('superlu_download', rfm.utility.udeps.fully)

    @require_deps
    def set_sourcedir(self, superlu_download):
        self.sourcesdir = os.path.join(
            superlu_download(part='local', environ='gnu7').stagedir,
            'superlu-5.2.2'
        )

    @run_before('compile')
    def set_build_system_attrs(self):
        self.build_system.max_concurrency = 8

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_not_found('error', self.stderr)


@rfm.simple_test
class autotools_build_test(rfm.RegressionTest):
    """
    run the amhello example and validate output
    """
    valid_systems = ['*:local-build', '*:slurm-build']
    valid_prog_environs = ['gnu7', 'gnu10', 'arm20.3', 'arm20.1']
    sourcepath = 'automake'
    build_system = 'Autotools'

    prebuild_cmds = ['autoreconf --install automake']
    postrun_cmds = ['./automake/src/hello']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found(r'Hello World\!', self.stdout)


@rfm.simple_test
class ninja_build(rfm.RunOnlyRegressionTest):
    """
    Run ninja-powered compile test
    """
    descr = 'ninja build test'
    valid_systems = ['*:local-build', '*:slurm-build']
    valid_prog_environs = ['gnu7']
    modules = ['ninja/1.8.2']
    executable = 'ninja'
    postrun_cmds = ['ls']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found('hello.o', self.stdout)


@rfm.simple_test
class bzip2_compression(rfm.RunOnlyRegressionTest):
    """
    Compress file using bzip2
    """
    descr = 'bzip2 compression test'
    valid_systems = ['*:local']
    valid_prog_environs = ['gnu7']
    modules = ['bzip2/1.0.6']
    executable = 'bzip2'
    executable_opts = ['-k', 'hello.c']
    postrun_cmds = ['ls']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found('hello.c.bz2', self.stdout)


@rfm.simple_test
class bzip2_decompression(rfm.RunOnlyRegressionTest):
    """
    Decompress previously compressed file using bzip2
    """
    descr = 'bzip2 decompression test'
    valid_systems = ['*:local']
    valid_prog_environs = ['gnu7']
    modules = ['bzip2/1.0.6']
    executable = 'bzip2'
    executable_opts = ['-d', 'hello.c.bz2']
    prerun_cmds = ['rm -f hello.c']
    postrun_cmds = ['ls']

    @run_after('init')
    def inject_dependencies(self):
        self.depends_on('bzip2_compression', rfm.utility.udeps.fully)

    @require_deps
    def set_sourcedir(self, bzip2_compression):
        self.sourcesdir = bzip2_compression(part='local', environ='gnu7').stagedir

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.all([
            sn.assert_not_found('hello.c.bz2', self.stdout),
            sn.assert_found('hello.c', self.stdout)
        ])


@rfm.simple_test
class xz_compression(rfm.RunOnlyRegressionTest):
    """
    Compress file using xz
    """
    descr = 'xz compression test'
    valid_systems = ['*:local']
    valid_prog_environs = ['gnu7']
    executable = 'xz'
    executable_opts = ['-k', 'hello.c']
    postrun_cmds = ['ls']
    modules = ['xz/5.2.4']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found('hello.c.xz', self.stdout)


@rfm.simple_test
class xz_decompression(rfm.RunOnlyRegressionTest):
    """
    Decompress previously compressed file using xz
    """
    descr = 'xz decompression test'
    valid_systems = ['*:local']
    valid_prog_environs = ['gnu7']
    modules = ['xz/5.2.4']
    executable = 'xz'
    executable_opts = ['-d', 'hello.c.xz']
    prerun_cmds = ['rm -f hello.c']
    postrun_cmds = ['ls']

    @run_after('init')
    def inject_dependencies(self):
        self.depends_on('xz_compression', rfm.utility.udeps.fully)

    @require_deps
    def set_sourcedir(self, xz_compression):
        self.sourcesdir = xz_compression(part='local', environ='gnu7').stagedir

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.all([
            sn.assert_not_found('hello.c.xz', self.stdout),
            sn.assert_found('hello.c', self.stdout)
        ])


@rfm.simple_test
class lua_filesystem(rfm.RunOnlyRegressionTest):
    """
    Run a lua program that uses the lua-luafilesystem
    package
    """
    descr = 'Run lua-luafilesystem test'
    valid_systems = ['*:local']
    valid_prog_environs = ['gnu7']
    modules = ['lua/5.3.5', 'lua-luafilesystem/1_7_0_2']
    executable = 'lua'
    executable_opts = ['lfs.lua']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found(r'./lfs.lua', self.stdout)

@rfm.simple_test
class lua_posix(rfm.RunOnlyRegressionTest):
    """
    Run a lua program that uses the lua-luaposix
    package
    """
    descr = 'Run lua-luaposix test'
    valid_systems = ['*:local']
    valid_prog_environs = ['gnu7']
    modules = ['lua/5.3.5', 'lua-luaposix/33.4.0']
    executable = 'lua'
    executable_opts = ['posix.lua']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.all([
            sn.assert_found(r'start', self.stdout),
            sn.assert_found(r'\n1', self.stdout),
            sn.assert_not_found('finish', self.stdout)
        ])

@rfm.simple_test
class yaml_cpp(rfm.RegressionTest):
    """
    Compile a simple cpp program
    that links against yaml-cpp
    """
    valid_systems = ['*:local_build', '*:slurm-build']
    valid_prog_environs = ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
    modules = ['yaml-cpp/0.6.2']
    sourcepath = 'yaml.cpp'
    build_system = 'SingleSource'

    @run_before('compile')
    def set_compilation_flags(self):
        environ = self.current_environ.name
        if environ == 'gnu7':
            comp = 'gcc@7.2.0'
        elif environ == 'gnu10':
            comp = 'gcc@10.3.0'
        elif environ == 'arm20.3':
            comp = '%arm'
        elif environ == 'arm21.1':
            comp = '%arm'
        yaml_path = rfm.utility.osext.run_command('spack location -i yaml-cpp %'+comp).stdout.rstrip()
        self.build_system.ldflags = ['-I'+yaml_path+'/include/', '-L'+yaml_path+'/lib/', '-lyaml-cpp']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_not_found(r'failed', self.stdout)


@rfm.simple_test
class qthreads_hello_world(rfm.RegressionTest):
    """
    Compile a simple cpp program
    that links against qthreads
    """
    valid_systems = ['*:local-build', '*:slurm-build']
    valid_prog_environs = ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
    modules = ['qthreads/1.14']
    sourcepath = 'qthreads_hello.c'
    build_system = 'SingleSource'

    @run_before('compile')
    def set_compilation_flags(self):
        environ = self.current_environ.name
        if environ == 'gnu7':
            comp = 'gcc@7.2.0'
        elif environ == 'gnu10':
            comp = 'gcc@10.3.0'
        elif environ == 'arm20.3':
            comp = '%arm'
        elif environ == 'arm21.1':
            comp = '%arm'
        lib_path = rfm.utility.osext.run_command('spack location -i qthreads %'+comp).stdout.rstrip()
        self.build_system.ldflags = ['-I'+lib_path+'/include/', '-L'+lib_path+'/lib/', '-lqthread']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found(r'Hello, world!', self.stdout)


@rfm.simple_test
class zlib_compression(rfm.RegressionTest):
    """
    Compile a c program that links against zlib
    """
    valid_systems = ['*:local-build', '*:slurm-build']
    valid_prog_environs = ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
    modules = ['zlib/1.2.11']
    sourcepath = 'zlib_zpipe.c'
    build_system = 'SingleSource'
    executable_opts = ['<zlib_zpipe.c>', 'zlib_zpipe.c.z']
    postrun_cmds = ['ls']

    @run_before('compile')
    def set_compilation_flags(self):
        environ = self.current_environ.name
        if environ == 'gnu7':
            lib_path = rfm.utility.osext.run_command('spack location -i zlib %gcc@7.2.0').stdout.rstrip()
        elif environ == 'gnu10':
            lib_path = rfm.utility.osext.run_command('spack location -i zlib %gcc@10.3.0').stdout.rstrip()
        else:
            lib_path = rfm.utility.osext.run_command('spack location -i zlib %arm').stdout.rstrip()

        self.build_system.ldflags = ['-I'+lib_path+'/include/', '-L'+lib_path+'/lib/', '-lz']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found(r'zlib_zpipe.c.z', self.stdout)


@rfm.simple_test
class zlib_decompression(rfm.RegressionTest):
    """
    Compile a c program that links against zlib
    """
    valid_systems = ['*']
    valid_prog_environs = ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
    modules = ['zlib/1.2.11']
    sourcepath = 'zlib_zpipe.c'
    build_system = 'SingleSource'
    executable_opts = ['<zlib_zpipe.c>', 'zlib_zpipe.c.z']
    postrun_cmds = ['rm -f zlib_zpipe.c',
                    './zlib_compression -d <zlib_zpipe.c.z> zlib_zpipe.c',
                    'ls'
    ]

    @run_after('init')
    def inject_dependencies(self):
        self.depends_on('zlib_compression', rfm.utility.udeps.fully)

    @run_before('compile')
    def set_compilation_flags(self):
        environ = self.current_environ.name
        if environ == 'gnu7':
            lib_path = rfm.utility.osext.run_command('spack location -i zlib %gcc@7.2.0').stdout.rstrip()
        elif environ == 'gnu10':
            lib_path = rfm.utility.osext.run_command('spack location -i zlib %gcc@10.3.0').stdout.rstrip()
        else:
            lib_path = rfm.utility.osext.run_command('spack location -i zlib %arm').stdout.rstrip()

        self.build_system.ldflags = ['-I'+lib_path+'/include/', '-L'+lib_path+'/lib/', '-lz']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found(r'zlib_zpipe.c.z', self.stdout)


@rfm.simple_test
class git_init(rfm.RunOnlyRegressionTest):
    """
    Runs git init in an empty directory to determine
    if git is working correctly
    """
    valid_systems = ['*:local']
    valid_prog_environs = ['*']
    modules = ['git/2.19.2']

    executable = 'git'
    executable_opts = ['init']

    prerun_cmds = ['mkdir git-test', 'cd git-test']
    postrun_cmds = ['git status']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found('On branch master', self.stdout)


@rfm.simple_test
class valgrind(rfm.RunOnlyRegressionTest):
    """
    valgrind memory debug test
    """
    valid_systems = ['*:local']
    valid_prog_environs = ['gnu7']
    modules = ['valgrind/3.15.0']

    executable = 'valgrind'
    executable_opts = ['--leak-check=yes', './a.out']

    prerun_cmds = ['gcc -g -O0 valgrind.c']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found('definitely lost: 40 bytes in 1 blocks', self.stderr)


@rfm.simple_test
class pmix_hello(rfm.RegressionTest):
    """
    pmix compile and hello world example
    """
    valid_systems = ['*:slurm-mpi']
    valid_prog_environs = ['gnu7', 'gnu10']
    modules = ['pmix/2.2.3']
    build_system = 'SingleSource'
    sourcepath = 'pmix/hello.c'

    # Request 50 nodes to ensure collective operations
    # have room to show off performance
    extra_resources = {'nodes': {'num_nodes': '4'},
                       'time': {'time': '0-04:00:00'}}

    num_tasks = 4

    @run_before('compile')
    def set_compilation_flags(self):
        environ = self.current_environ.name
        if environ == 'gnu7':
            lib_path = rfm.utility.osext.run_command('spack location -i pmix %gcc@7.2.0').stdout.rstrip()
        elif environ == 'gnu10':
            lib_path = rfm.utility.osext.run_command('spack location -i pmix %gcc@10.3.0').stdout.rstrip()
        elif environ == 'arm20.3':
            lib_path = rfm.utility.osext.run_command('spack location -i pmix %arm').stdout.rstrip()
        elif environ == 'arm21.1':
            lib_path = rfm.utility.osext.run_command('spack location -i pmix %arm').stdout.rstrip()

        self.build_system.ldflags = ['-I'+lib_path+'/include/', '-L'+lib_path+'/lib/', '-lpmix']

    @run_before('run')
    def set_core_opts(self):
        self.job.launcher.options = ['-np', '4', '-mca', 'pml', '^ucx', '-mca', 'coll_hcoll_enable', '0', '-map-by node']

    @run_before('sanity')
    def set_sanity_patterns(self):
        num_messages = sn.len(sn.findall(r'rank \d:PMIx_Finalize successfully completed',
                                         self.stderr))
        self.sanity_patterns = sn.assert_eq(num_messages, 4)


@rfm.simple_test
class netcdf_write_c(rfm.RegressionTest):
    """
    Compile a netcdf-c program that writes an example program
    """
    valid_systems = ['*:local-build', '*:slurm-build']
    valid_prog_environs = ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
    modules = ['openmpi/3.1.4','netcdf-c/4.6.3']
    sourcepath = 'netcdf_xy_wr.c'
    build_system = 'SingleSource'

    @run_before('compile')
    def set_compilation_flags(self):
        environ = self.current_environ.name
        if environ == 'gnu7':
            lib_path = rfm.utility.osext.run_command('spack location -i netcdf-c %gcc@7.2.0').stdout.rstrip()
        elif environ == 'gnu10':
            lib_path = rfm.utility.osext.run_command('spack location -i netcdf-c %gcc@10.3.0').stdout.rstrip()
        elif environ == 'arm20.3':
            lib_path = rfm.utility.osext.run_command('spack location -i netcdf-c %arm').stdout.rstrip()
        elif environ == 'arm21.1':
            lib_path = rfm.utility.osext.run_command('spack location -i netcdf-c %arm').stdout.rstrip()

        self.build_system.ldflags = ['-I'+lib_path+'/include/', '-L'+lib_path+'/lib/', '-lnetcdf']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found(r'SUCCESS writing example file simple_xy.nc!', self.stdout)


@rfm.simple_test
class netcdf_read_c(rfm.RegressionTest):
    """
    Compile a netcdf-c program that writes an example program
    """
    valid_systems = ['*:local-build', '*:slurm-build']
    valid_prog_environs = ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
    modules = ['openmpi/3.1.4', 'netcdf-c/4.6.3']
    sourcepath = 'netcdf_xy_rd.c'
    build_system = 'SingleSource'

    @run_after('init')
    def inject_dependencies(self):
        self.depends_on('netcdf_write_c', rfm.utility.udeps.fully)

    @require_deps
    def set_sourcedir(self, netcdf_write_c):
        self.sourcesdir = os.path.join(
            netcdf_write_c(part='local-build', environ=self.current_environ.name).stagedir
        )

    @run_before('compile')
    def set_compilation_flags(self):
        environ = self.current_environ.name
        if environ == 'gnu7':
            lib_path = rfm.utility.osext.run_command('spack location -i netcdf-c %gcc@7.2.0').stdout.rstrip()
        elif environ == 'gnu10':
            lib_path = rfm.utility.osext.run_command('spack location -i netcdf-c %gcc@10.3.0').stdout.rstrip()
        elif environ == 'arm20.3':
            lib_path = rfm.utility.osext.run_command('spack location -i netcdf-c %arm').stdout.rstrip()
        elif environ == 'arm21.1':
            lib_path = rfm.utility.osext.run_command('spack location -i netcdf-c %arm').stdout.rstrip()

        self.build_system.ldflags = ['-I'+lib_path+'/include/', '-L'+lib_path+'/lib/', '-lnetcdf']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found(r'SUCCESS reading example file simple_xy.nc!', self.stdout)

@rfm.simple_test
class netcdf_write_cpp(rfm.RegressionTest):
    """
    Compile a netcdf-cxx program that writes an example program
    """
    valid_systems = ['*:local-build', '*:slurm-build']
    valid_prog_environs = ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
    modules = ['openmpi/3.1.4', 'netcdf-cxx4/4.3.0']
    sourcepath = 'netcdf_xy_wr.cpp'
    build_system = 'SingleSource'

    @run_before('compile')
    def set_compilation_flags(self):
        environ = self.current_environ.name
        if environ == 'gnu7':
            lib_path = rfm.utility.osext.run_command('spack location -i netcdf-cxx4 %gcc@7.2.0').stdout.rstrip()
        elif environ == 'gnu10':
            lib_path = rfm.utility.osext.run_command('spack location -i netcdf-cxx4 %gcc@10.3.0').stdout.rstrip()
        elif environ == 'arm20.3':
            lib_path = rfm.utility.osext.run_command('spack location -i netcdf-cxx4 %arm').stdout.rstrip()
        elif environ == 'arm21.1':
            lib_path = rfm.utility.osext.run_command('spack location -i netcdf-cxx4 %arm').stdout.rstrip()

        self.build_system.ldflags = ['-I'+lib_path+'/include/', '-L'+lib_path+'/lib/', '-lnetcdf']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found(r'SUCCESS writing example file simple_xy.nc!', self.stdout)


@rfm.simple_test
class netcdf_read_cpp(rfm.RegressionTest):
    """
    Compile a netcdf-cxx program that writes an example program
    """
    valid_systems = ['*:local-build', '*:slurm-build']
    valid_prog_environs = ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
    modules = ['openmpi/3.1.4', 'netcdf-cxx4/4.3.0']
    sourcepath = 'netcdf_xy_rd.cpp'
    build_system = 'SingleSource'

    @run_after('init')
    def inject_dependencies(self):
        self.depends_on('netcdf_write_cpp', rfm.utility.udeps.fully)

    @require_deps
    def set_sourcedir(self, netcdf_write_cpp):
        self.sourcesdir = os.path.join(
            netcdf_write_cpp(part='local-build', environ=self.current_environ.name).stagedir
        )

    @run_before('compile')
    def set_compilation_flags(self):
        environ = self.current_environ.name
        if environ == 'gnu7':
            lib_path = rfm.utility.osext.run_command('spack location -i netcdf-cxx4 %gcc@7.2.0').stdout.rstrip()
        elif environ == 'gnu10':
            lib_path = rfm.utility.osext.run_command('spack location -i netcdf-cxx4 %gcc@10.3.0').stdout.rstrip()
        elif environ == 'arm20.3':
            lib_path = rfm.utility.osext.run_command('spack location -i netcdf-cxx4 %arm').stdout.rstrip()
        elif environ == 'arm21.1':
            lib_path = rfm.utility.osext.run_command('spack location -i netcdf-cxx4 %arm').stdout.rstrip()

        self.build_system.ldflags = ['-I'+lib_path+'/include/', '-L'+lib_path+'/lib/', '-lnetcdf']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found(r'SUCCESS reading example file simple_xy.nc!', self.stdout)


@rfm.simple_test
class binutils_ld(rfm.RunOnlyRegressionTest):
    """
    Test binutils by manually linking hello world with libc
    """
    valid_systems = ['*:local-build', '*:slurm-build']
    valid_prog_environs = ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
    modules = ['binutils/2.37']
    sourcepath = 'hello.c'
    build_system = 'SingleSource'

    prerun_cmds = ['gcc -c -o hello.o hello.c']

    executable = 'ld'
    executable_opts = ['-o', 'output', 'hello.o', '-lc', '--entry', 'main']

    postrun_cmds = ['ls']

    @run_before('compile')
    def set_compilation_flags(self):
        self.build_system.cflags = ['-c', '-o', 'hello.o']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.all([
                sn.assert_found('output', self.stdout),
                sn.assert_not_found('error', self.stderr)
            ])


@rfm.simple_test
class metis(rfm.RegressionTest):
    """
    Compile a cpp program
    that links against metis
    """
    valid_systems = ['*:local-build', '*:slurm-build']
    valid_prog_environs = ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
    modules = ['metis/5.1.0']
    sourcepath = 'metis_test.cpp'
    build_system = 'SingleSource'

    @run_before('compile')
    def set_compilation_flags(self):
        environ = self.current_environ.name
        if environ == 'gnu7':
            comp = 'gcc@7.2.0'
        elif environ == 'gnu10':
            comp = 'gcc@10.3.0'
        elif environ == 'arm20.3':
            comp = '%arm'
        elif environ == 'arm21.1':
            comp = '%arm'

        lib_path = rfm.utility.osext.run_command('spack location -i metis %'+comp).stdout.rstrip()
        self.build_system.ldflags = ['-I'+lib_path+'/include/', '-L'+lib_path+'/lib/', '-lmetis']

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_not_found('error', self.stderr)
