#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {

    MPI_Init(&argc, &argv);

    int rank, size;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int msg;
    int final = 0;

    if (rank == 0) {
        msg = 1;
    } else {
        msg = 0;
    }

    if (rank == 0) {
        MPI_Send(&msg, 1, MPI_INT, rank+1, 0, MPI_COMM_WORLD);
        MPI_Recv(&final, 1, MPI_INT, size-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    } else if (rank == size-1) {
        MPI_Recv(&msg, 1, MPI_INT, rank-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Send(&msg, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);

    } else {
        MPI_Recv(&msg, 1, MPI_INT, rank-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Send(&msg, 1, MPI_INT, rank+1, 0, MPI_COMM_WORLD);
    }


    if (rank == 0 && final == 1) {
        printf("Ring Complete\n");
    } else if (rank == 0 && final != 1) {
        printf("Ring Failed\n");
    }


    MPI_Finalize();

    return 0;
}
