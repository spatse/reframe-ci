#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {

    // Initialize MPI Environment
    MPI_Init(&argc, &argv);

    // Define rank and size are processor id and processor count respectively
    int rank, size;

    // Initialize rank and size with values
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // Print Hello World from each processor
    // (rank is increased by 1 since rank starts from
    // zero and size is the # of processors )
    printf("Hello from processor %d of %d\n", rank+1, size);

    // Finalize MPI Environment
    MPI_Finalize();

    return 0;
}
