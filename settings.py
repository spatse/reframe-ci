# ReFrame settings configuration for the
# Advanced Tri-Lab Software Environment (ATSE)
# Written and maintained by Carson Woods

site_configuration = {
    'systems': [
        {
            'name': 'stria',
            'descr': 'functionality testing for ATSE',
            'hostnames': ['st*'],
            'modules_system': 'lmod',
            'partitions': [
                {
                    'name': 'local',
                    'scheduler': 'local',
                    'launcher': 'local',
                    'max_jobs': 24,
                    'environs': ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
                },
                {
                    'name': 'local-build',
                    'scheduler': 'local',
                    'launcher': 'local',
                    'environs': ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
                },
                {
                    'name': 'cuda',
                    'scheduler': 'slurm',
                    'launcher': 'srunalloc',
                    'access': ['-A fy190142'],
                    'resources': [
                        {
                            'name': 'partition',
                            'options': ['--partition={partition}']
                        },
                        {
                            'name': 'time',
                            'options': ['--time={time}']
                        }
                    ],
                    'environs': ['cuda10.2']
                },
                {
                    'name': 'mpi',
                    'scheduler': 'local',
                    'launcher': 'mpirun',
                    'max_jobs': 20,
                    'environs': ['openmpi3', 'openmpi4']
                },
                {
                    'name': 'slurm',
                    'scheduler': 'slurm',
                    'launcher': 'srun',
                    'access': ['-A fy190142'],
                    'max_jobs': 50,
                    'resources': [
                        {
                            'name': 'nodes',
                            'options': ['--nodes={num_nodes}']
                        }
                    ],
                    'environs': ['gnu7', 'gnu10', 'arm20.3', 'arm21.1', 'openmpi3', 'openmpi4']
                },
                {
                    'name': 'slurm-build',
                    'scheduler': 'slurm',
                    'launcher': 'srun',
                    'access': ['-A fy190142'],
                    'max_jobs': 50,
                    'resources': [
                        {
                            'name': 'nodes',
                            'options': ['--nodes={num_nodes}']
                        }
                    ],
                    'environs': ['gnu7', 'gnu10', 'openmpi3', 'openmpi4']
                },
                {
                    'name': 'slurm-mpi',
                    'scheduler': 'slurm',
                    'launcher': 'mpirun',
                    'access': ['-A fy190142'],
                    'max_jobs': 50,
                    'resources': [
                        {
                            'name': 'nodes',
                            'options': ['--nodes=8']
                        },
                        {
                            'name': 'time',
                            'options': ['--time=0-04:00:00']
                        }
                    ],
                    'environs': ['gnu7', 'gnu10', 'arm20.3', 'arm21.1', 'openmpi3', 'openmpi4']
                },
            ]
        },
        {
            'name': 'mayer',
            'descr': 'functionality testing for ATSE',
            'hostnames': ['may*'],
            'modules_system': 'lmod',
            'partitions': [
                {
                    'name': 'local',
                    'scheduler': 'local',
                    'launcher': 'local',
                    'environs': ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
                },
                {
                    'name': 'local-build',
                    'scheduler': 'local',
                    'launcher': 'local',
                    'environs': ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
                },
                {
                    'name': 'mpi',
                    'scheduler': 'local',
                    'launcher': 'mpiexec',
                    'environs': ['openmpi']
                },
                {
                    'name': 'slurm',
                    'scheduler': 'slurm',
                    'launcher': 'srun',
                    'access': ['-A fy190142'],
                    'environs': ['gnu7', 'gnu10', 'arm20.3', 'arm21.1']
                },
                {
                    'name': 'slurm-build',
                    'scheduler': 'slurm',
                    'launcher': 'srun',
                    'max_jobs': 50,
                    'resources': [
                        {
                            'name': 'nodes',
                            'options': ['--nodes={num_nodes}']
                        }
                    ],
                    'environs': ['gnu7', 'gnu10', 'openmpi3', 'openmpi4']
                },
            ]
        },
        {
            'name': 'inouye',
            'descr': 'Inouye settings for SPATSE',
            'hostnames': ['inouye*'],
            'modules_system': 'lmod',
            'partitions': [
                {
                    'name': 'local',
                    'scheduler': 'local',
                    'launcher': 'local',
                    'max_jobs': 24,
                    'environs': ['gnu7', 'gnu10', 'fj4.0', 'fj4.0', 'fj4.5']
                },
                {
                    'name': 'local-build',
                    'scheduler': 'local',
                    'launcher': 'local',
                    'environs': ['gnu7', 'gnu10', 'fj4.0', 'fj4.0', 'fj4.5']
                },
                {
                    'name': 'mpi',
                    'scheduler': 'local',
                    'launcher': 'mpirun',
                    'max_jobs': 20,
                    'environs': ['openmpi3', 'openmpi4']
                },
                {
                    'name': 'slurm',
                    'scheduler': 'slurm',
                    'launcher': 'srun',
                    'access': ['-A fy190142'],
                    'max_jobs': 50,
                    'resources': [
                        {
                            'name': 'nodes',
                            'options': ['--nodes={num_nodes}']
                        }
                    ],
                    'environs': ['gnu7', 'gnu10', 'fj4.0', 'fj4.0', 'fj4.5', 'openmpi3', 'openmpi4']
                },
                {
                    'name': 'slurm-build',
                    'scheduler': 'slurm',
                    'launcher': 'srun',
                    'access': ['-A fy190142'],
                    'max_jobs': 50,
                    'resources': [
                        {
                            'name': 'nodes',
                            'options': ['--nodes={num_nodes}']
                        }
                    ],
                    'environs': ['gnu7', 'gnu10', 'fj4.0', 'fj4.0', 'fj4.5', 'openmpi3', 'openmpi4']
                },
                {
                    'name': 'slurm-mpi',
                    'scheduler': 'slurm',
                    'launcher': 'mpirun',
                    'access': ['-A fy190142'],
                    'max_jobs': 50,
                    'resources': [
                        {
                            'name': 'nodes',
                            'options': ['--nodes=8']
                        },
                        {
                            'name': 'time',
                            'options': ['--time=0-04:00:00']
                        }
                    ],
                    'environs': ['gnu7', 'gnu10', 'fj4.0', 'fj4.0', 'fj4.5', 'openmpi3', 'openmpi4']
                },
            ]
        },
        {
            'name': 'Gitlab Runners',
            'descr': 'CI testing for ATSE',
            'hostnames': ['runner*'],
            'modules_system': 'spack',
            'partitions': [
                {
                    'name': 'local',
                    'scheduler': 'local',
                    'launcher': 'local',
                    'environs': ['gnu']
                }
            ]
        },
    ],
    'environments': [
        {
            'name': 'gnu',
            'cc': 'gcc',
            'cxx': 'g++',
            'ftn': 'gfortran'
        },
        {
            'name': 'gnu10',
            'modules': ['gcc/10.3.0'],
            'cc': 'gcc',
            'cxx': 'g++',
            'ftn': 'gfortran'
        },
        {
            'name': 'gnu7',
            'modules': ['gcc/7.2.0'],
            'cc': 'gcc',
            'cxx': 'g++',
            'ftn': 'gfortran'
        },
        {
            'name': 'clang',
            'cc': 'clang',
            'cxx': 'clang++',
            'ftn': 'flang'
        },
        {
            'name': 'arm20.3',
            'modules': ['arm/20.3'],
            'cc': 'armclang',
            'cxx': 'armclang++',
            'ftn': 'armflang',
        },
        {
            'name': 'arm21.1',
            'modules': ['arm/21.1'],
            'cc': 'armclang',
            'cxx': 'armclang++',
            'ftn': 'armflang',
        },
        {
            'name': 'openmpi3',
            'modules': ['gcc/7.2.0', 'openmpi/3.1.4'],
            'cc': 'mpicc',
            'cxx': 'mpicxx',
            'ftn': 'mpif90',
        },
        {
            'name': 'openmpi4',
            'modules': ['gcc/7.2.0', 'openmpi/4.0.5'],
            'cc': 'mpicc',
            'cxx': 'mpicxx',
            'ftn': 'mpif90',
        },
        {
            'name': 'cuda10.2',
            'cc': 'nvcc',
            'cxx': 'nvcc',
        },
        {
            'name': 'fj4.0',
            'modules': ['fj/4.0.0'],
            'cc': 'fcc',
            'cxx': 'FCC',
        },
        {
            'name': 'fj4.2',
            'modules': ['fj/4.2.0'],
            'cc': 'fcc',
            'cxx': 'FCC',
        },
        {
            'name': 'fj4.5',
            'modules': ['fj/4.5.0'],
            'cc': 'fcc',
            'cxx': 'FCC',
        }
    ],
    'logging': [
        {
            'handlers': [
                {
                    'type': 'stream',
                    'name': 'stdout',
                    'level': 'info',
                    'format': '%(message)s'
                },
                {
                    'type': 'file',
                    'level': 'debug',
                    'format': '[%(asctime)s] %(levelname)s: %(check_info)s: %(message)s',   # noqa: E501
                    'append': False
                }
            ],
            'handlers_perflog': [
                {
                    'type': 'filelog',
                    'prefix': '%(check_system)s/%(check_partition)s',
                    'level': 'info',
                    'format': (
                        '%(check_job_completion_time)s|reframe %(version)s|'
                        '%(check_info)s|jobid=%(check_jobid)s|'
                        '%(check_perf_var)s=%(check_perf_value)s|'
                        'ref=%(check_perf_ref)s '
                        '(l=%(check_perf_lower_thres)s, '
                        'u=%(check_perf_upper_thres)s)|'
                        '%(check_perf_unit)s'
                    ),
                    'append': True
                }
            ]
        }
    ],
}
