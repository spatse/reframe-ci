# ReFrame test pipeline for the
# Advanced Tri-Lab Software Environment (ATSE)
# Written and maintained by Carson Woods

import reframe as rfm
import reframe.utility.sanity as sn


@rfm.simple_test
class Hello_World_C(rfm.RegressionTest):
    """
    Compile a simple c program
    and test execution output
    """
    valid_systems = ['*']
    valid_prog_environs = ['gnu']
    sourcepath = 'hello.c'

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found(r'Hello, World\!', self.stdout)


@rfm.simple_test
class Hello_World_CPP(rfm.RegressionTest):
    """
    Compile a simple cpp program
    and test execution output
    """
    valid_systems = ['*']
    valid_prog_environs = ['gnu']
    sourcepath = 'hello.cpp'

    @run_before('sanity')
    def set_sanity_patterns(self):
        self.sanity_patterns = sn.assert_found(r'Hello, World\!', self.stdout)


@rfm.simple_test
class Hello_Threaded_CPP(rfm.RegressionTest):
    """
    Compile a multithreaded cpp program
    using pthread and test execution output
    """
    valid_systems = ['*']
    valid_prog_environs = ['gnu']
    sourcepath = 'hello_threads.cpp'
    build_system = 'SingleSource'
    executable_opts = ['16']

    @run_before('compile')
    def set_compilation_flags(self):
        self.build_system.cppflags = ['-DSYNC_MESSAGES']
        self.build_system.cxxflags = ['-std=c++11', '-Wall']
        environ = self.current_environ.name
        if environ in {'gnu'}:
            self.build_system.cxxflags += ['-pthread']

    @run_before('sanity')
    def set_sanity_patterns(self):
        num_messages = sn.len(sn.findall(r'\[\s?\d+\] Hello, World\!',
                                         self.stdout))
        self.sanity_patterns = sn.assert_eq(num_messages, 16)
