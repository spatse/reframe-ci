# ATSE Test Pipeline
The ATSE testing pipeline uses Reframe to execute the test environment.

## Supported Machines
Currently, the testing pipeline has settings written for the following machines:
- Stria
- Mayer

When a machine is "supported", this means that settings, hardware capabilities, partitions, programming environments, etc. have been defined in the `settings.py`.
If you have a custom `settings.py` file with settings for additional machine, then these tests will run so long as ATSE is installed on that machine.

## Install Reframe
Use one of the listed methods below.

**NOTE:** Using Spack will require loading the package before each use (or in a `.bashrc` file).

#### Using Pip:

```pip install reframe-hpc```

#### Using Spack:

```spack install reframe```

#### Using EasyBuild:

```eb ReFrame-VERSION.eb -r```

#### From Source:

**NOTE:** Substitute in an installation prefix and version tag into below script.
```shell
pushd /path/to/install/prefix
git clone -q --depth 1 --branch VERSION_TAG https://github.com/eth-cscs/reframe.git
pushd reframe && ./bootstrap.sh && popd
export PATH=$(pwd)/bin:$PATH
popd
```

## Running Tests

For the sake of the below instructions, assume that `$ATSE_SPACK_DIR` corresponds to the path where this repo was cloned to.

### Running All Tests:
From within the `$ATSE_SPACK_DIR/tests` directory, run `./test.sh` to launch all tests.

Doing so will launch the following command:

`reframe -C settings.py -c test-atse.py -r`

### Running Specific Tests:

**To run a specific test name, use the following command:**

`reframe -C settings.py -c test-atse.py -r -n [TEST_NAME]`

Where [TEST_NAME] is substituted for the desired test.

**To run tests on a specific programming environment, use the following command:**

`reframe -C settings.py -c test-atse.py -r -p [PRG_ENV_NAME]`

Where [PRG_ENV_NAME] is substituted for the desired programming environment.

**NOTE:** a combination of `-n [TEST_NAME]` and `-p [PRG_ENV_NAME]` can be used to choose a very specific test. If name is left unspecified, then all tests will run but only on specified programming environment. If a name is specified, then only that test will run, but on every programming environment.

**To keep staged test after execution, use the following command:**

`reframe -C settings.py -c test-atse.py -r --keep-stage-files`

### Specifying Settings Files:
To specify a different settings file, replace the `settings.py` file in the `$ATSE_SPACK_DIR/tests` directory, or create a differently named file, and place the name in the `-C [SETTING_FILE_NAME].py` portion of the reframe test command.

### Specifying Tests Files:
To specify a different tests file, replace the `test-atse.py` file in the `$ATSE_SPACK_DIR/tests` directory, or create a differently named file, and place the name in the `-c [TEST_FILE_NAME].py` portion of the reframe test command.
